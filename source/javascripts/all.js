// This is where it all goes :)
$(document).ready(function(){
  $('#about-me .container').waypoint(function() {
      $('#about-me .container').css('visibility', 'visible');
      $('#about-me .container').addClass('animated fadeInLeft');
  }, { offset: 'bottom-in-view' });

  $('#skills .container').waypoint(function() {
      $('#skills .container').css('visibility', 'visible');
      $('#skills .display-3').addClass('animated fadeInLeft');
      $('#skills .row').addClass('animated fadeInUp');
  }, { offset: 'right-in-view' });

  $('#links .container').waypoint(function() {
      $('#links .container').css('visibility', 'visible');
      $('#links .display-2').addClass('animated fadeInLeft');
      $('#links .row .card').addClass('animated fadeIn jello');
  }, { offset: 'right-in-view' });

});
