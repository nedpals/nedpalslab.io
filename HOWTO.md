# HOWTO: Make a website (*in Bisglish*)

__Note: In this tutorial, we will use Wix.com as our preferred site builder because it's free and easy.__

1. Register first your [Wix](https://wix.com) account. 
   __Please use the Facebook login method para mas dali ang process sa pag-create ug account.__
     
  

1. ![second step](howto/2.png) 
	Select what will be the purpose of your site. Kung wala kay maisip, click "others".
     
   
   
1. ![third step](howto/3.png)
	Kung tapulan ka maghimo, use the Wix ADI. But in this case, para naay effort, use the Wix editor by clicking the button at the right.
     
     
1. ![fourth step](howto/4.png)
	Pick a template that you liked. By placing the mouse cursor sa image sa napili nimo na theme, you can see two buttons. "Edit" lets you edit the template and "View" lets you view first the template before editing. Click the "Edit" button.
        
      
2. 	Skip the video by pressing "Start Now".
    
      
     
3.	Start edit the template and click "Publish" once you are done.
    
THAT'S ALL! If you have any questions, [please PM me](https://m.me/slapden). :smile: